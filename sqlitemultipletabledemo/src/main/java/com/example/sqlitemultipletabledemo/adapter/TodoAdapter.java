package com.example.sqlitemultipletabledemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitemultipletabledemo.R;
import com.example.sqlitemultipletabledemo.model.TodoModel;

import java.util.ArrayList;
import java.util.List;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.MyViewHolder>{

    private List<TodoModel> data;
    private Context mContext;
    private TodoAdapter.ItemClickListener mClickListener;

    public TodoAdapter(Context context, List<TodoModel> data) {
        this.mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewStatus = LayoutInflater.from(mContext).inflate(R.layout.item_todo, parent, false);
        return new MyViewHolder(viewStatus);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TodoModel model = data.get(position);
        holder.tvId.setText(String.valueOf(model.getId()));
        holder.tvBody.setText(String.valueOf(model.getNote()));
        holder.tvStatus.setText(String.valueOf(model.getStatus()));
        holder.tvDate.setText(String.valueOf(model.getCreated_at()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvDate;
        private TextView tvBody;
        private TextView tvStatus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvId);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvBody = itemView.findViewById(R.id.tvBody);
        }
    }


    void addData(TodoModel item) {
        data.add(item);
    }

    // allows clicks events to be caught
    public void setClickListener(TodoAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    //parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemAdapterClick(View view, int position);
    }
}
