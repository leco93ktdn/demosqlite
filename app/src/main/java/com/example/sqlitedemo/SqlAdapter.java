//package com.example.demokotlin;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.ContextMenu;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.demokotlin.model.Model;
//
//import java.util.List;
//
//public class SqlAdapter extends RecyclerView.Adapter<SqlAdapter.RecyclerViewHolder> {
//    private Context context;
//    private List<Model> data;
//    private static final int MENU_ITEM_VIEW = 111;
//    private static final int MENU_ITEM_EDIT = 222;
//    private static final int MENU_ITEM_CREATE = 333;
//    private static final int MENU_ITEM_DELETE = 444;
//    MyDataBaseHelper db;
//    ListActivity activity;
//    SqlAdapter(Context context, List<Model> data) {
//        this.context = context;
//        this.data = data;
//        db = new MyDataBaseHelper(context);
//        activity = new ListActivity();
//    }
//
//    @NonNull
//    @Override
//    public SqlAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View view = inflater.inflate(R.layout.item, parent, false);
//        return new RecyclerViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {
//        Model item = data.get(position);
//
//        holder.tvId.setText(String.valueOf(item.getId()));
//        holder.tvA.setText(String.valueOf(item.getA()));
//        holder.tvB.setText(String.valueOf(item.getB()));
//        holder.tvC.setText(String.valueOf(item.getC()));
//        holder.tvCv.setText(String.valueOf(item.getCv()));
//        holder.tvDt.setText(String.valueOf(item.getDt()));
////        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
////            @Override
////            public boolean onLongClick(View v) {
////                return false;
////            }
////        });
////
////        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
////            @Override
////            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
////                menu.add(0, MENU_ITEM_VIEW, 0, "View Note")
////                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
////                            @Override
////                            public boolean onMenuItemClick(MenuItem item) {
////                                holder.getAdapterPosition();
////                                return true;
////                            }
////                        });
////                menu.add(0, MENU_ITEM_CREATE, 1, "Create Note")
////                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
////                            @Override
////                            public boolean onMenuItemClick(MenuItem item) {
////                                holder.getAdapterPosition();
////                                Model a = data.get(position);
////                                return true;
////                            }
////                        });
////                menu.add(0, MENU_ITEM_EDIT, 2, "Edit Note")
////                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
////                            @Override
////                            public boolean onMenuItemClick(MenuItem item) {
////                                int positon = holder.getAdapterPosition();
////                                Model a = data.get(position);
////                                return true;
////                            }
////                        });
////                menu.add(0, MENU_ITEM_DELETE, 4, "Delete Note")
////                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
////                            @Override
////                            public boolean onMenuItemClick(MenuItem item) {
////                                holder.getAdapterPosition();
////                                Model a = data.get(position);
////                                db.deleteModel(a);
////                                notifyDataSetChanged();
////                                return true;
////                            }
////                        });
////            }
////
////        });
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return data.size();
//    }
//
//    static class RecyclerViewHolder extends RecyclerView.ViewHolder
////            implements View.OnClickListener, View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener
//    {
//
//        private TextView tvId, tvA, tvB, tvC, tvCv, tvDt;
//
//        RecyclerViewHolder(@NonNull View itemView) {
//            super(itemView);
//            tvId = itemView.findViewById(R.id.tvId);
//            tvA = itemView.findViewById(R.id.tvA);
//            tvB = itemView.findViewById(R.id.tvB);
//            tvC = itemView.findViewById(R.id.tvC);
//            tvCv = itemView.findViewById(R.id.tvCv);
//            tvDt = itemView.findViewById(R.id.tvDt);
//        }
//    }
//}

package com.example.sqlitedemo;

import android.app.Activity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.sqlitedemo.model.Model;

import java.util.List;

public class SqlAdapter extends RecyclerView.Adapter<SqlAdapter.RecyclerViewHolder> {
    private Activity context;
    private List<Model> data;
    private static final int MENU_ITEM_VIEW = 111;
    private static final int MENU_ITEM_EDIT = 222;
    private static final int MENU_ITEM_CREATE = 333;
    private static final int MENU_ITEM_DELETE = 444;
    MyDataBaseHelper db;

    SqlAdapter(Activity context, List<Model> data) {
        this.context = context;
        this.data = data;
        db = new MyDataBaseHelper(context);
    }

    @NonNull
    @Override
    public SqlAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {
        Model item = data.get(position);

        holder.tvId.setText(String.valueOf(item.getId()));
        holder.tvA.setText(String.valueOf(item.getA()));
        holder.tvB.setText(String.valueOf(item.getB()));
        holder.tvC.setText(String.valueOf(item.getC()));
        holder.tvCv.setText(String.valueOf(item.getCv()));
        holder.tvDt.setText(String.valueOf(item.getDt()));
//        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                return false;
//            }
//        });

        holder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0, MENU_ITEM_VIEW, 0, "View Note")
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                holder.getAdapterPosition();
                                return true;
                            }
                        });
                menu.add(0, MENU_ITEM_CREATE, 1, "Create Note")
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                holder.getAdapterPosition();
                                Model a = data.get(position);
                                return true;
                            }
                        });
                menu.add(0, MENU_ITEM_EDIT, 2, "Edit Note")
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                int positon = holder.getAdapterPosition();
                                Model a = data.get(position);
                                return true;
                            }
                        });
                menu.add(0, MENU_ITEM_DELETE, 4, "Delete Note")
                        .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                holder.getAdapterPosition();
                                Model a = data.get(position);
                                db.deleteModel(a);
                                data.remove(a);
                                context.recreate();
                                return true;
                            }
                        });
            }

        });

    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView tvId, tvA, tvB, tvC, tvCv, tvDt;

        RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvId);
            tvA = itemView.findViewById(R.id.tvA);
            tvB = itemView.findViewById(R.id.tvB);
            tvC = itemView.findViewById(R.id.tvC);
            tvCv = itemView.findViewById(R.id.tvCv);
            tvDt = itemView.findViewById(R.id.tvDt);
        }

//        @Override
//        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//            MenuItem Edit = menu.add(Menu.NONE, 1, 1, "Edit");
//            MenuItem Delete = menu.add(Menu.NONE, 2, 2, "Delete");
//            Edit.setOnMenuItemClickListener(onEditMenu);
//            Delete.setOnMenuItemClickListener(onEditMenu);
//        }

//        private final MenuItem.OnMenuItemClickListener onEditMenu = new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//
//
////                DBHandler dbHandler = new DBHandler(ctx);
////                List<WishMen> data = dbHandler.getWishmen();
//                int position = item.getMenuInfo().
//                switch (item.getItemId()) {
//                    case 1:
//                        //Do stuff
//                        break;
//
//                    case 2:
//                        //Do stuff
//                        delete();
//
//                }
//                return true;
//            }
//        };
//
//
    }
//    private void delete() {
//        new AlertDialog.Builder(context)
//                .setMessage(selectedModel.getId() + ". Are you sure you want to delete?")
//                .setCancelable(false)
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        deleteModel(selectedModel);
//                    }
//                })
//                .setNegativeButton("No", null)
//                .show();
//        break;
//    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public List<Model> getData() {
        return data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}