package com.example.sqlitemultipletabledemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.sqlitemultipletabledemo.adapter.TagAdapter;
import com.example.sqlitemultipletabledemo.adapter.TodoAdapter;
import com.example.sqlitemultipletabledemo.helper.DatabaseHelper;
import com.example.sqlitemultipletabledemo.model.TagModel;
import com.example.sqlitemultipletabledemo.model.TodoModel;

import java.util.ArrayList;
import java.util.List;

public class TagActivity extends AppCompatActivity {
    private ImageButton btnBack;
    private EditText editText;
    private Button insert, update, delete;
    private RecyclerView rvTags;
    private DatabaseHelper db;
    private List<TagModel> tagList = new ArrayList<>();
    private TagAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);

        db = new DatabaseHelper(getApplicationContext());

        tagList = db.getAllTag();
        initView();
        showTagList(tagList);
        initEvent();
    }

    private void showTagList(List<TagModel> tagList) {
        Log.e("TAGLIST", tagList.size() + "");
        if (tagList.size() > 0) {
            adapter = new TagAdapter(this, tagList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvTags.setLayoutManager(linearLayoutManager);
//        adapter.setClickListener();
            rvTags.setAdapter(adapter);
        }
    }

    private void initEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter your todo", Toast.LENGTH_LONG).show();
                } else {
                    String note = editText.getText().toString();
                    TagModel tagItem = new TagModel(note);
                    db.createTag(tagItem);
                    tagList = db.getAllTag();
                    showTagList(tagList);
                    editText.setText("");
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (TagModel model : tagList) {
                    db.deleteTag(model, false);
                }
                tagList.clear();
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void initView() {
        btnBack = findViewById(R.id.btnBack);
        editText = findViewById(R.id.editText);
        insert = findViewById(R.id.button);
        update = findViewById(R.id.button2);
        delete = findViewById(R.id.button3);
        rvTags = findViewById(R.id.rvTags);
    }
}
