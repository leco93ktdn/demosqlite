package com.example.sqlitedemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.example.sqlitedemo.model.Model;

public class AddEditModelActivity extends AppCompatActivity {
    TextView textView, textView2;
    EditText editText, editText2, editText3;
    Button button;

    private static final int MODE_CREATE = 1;
    private static final int MODE_EDIT = 2;

    private Model models;
    private boolean needRefresh;
    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_model);

        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);
        editText = findViewById(R.id.editText);
        editText2 = findViewById(R.id.editText2);
        editText3 = findViewById(R.id.editText3);
        button = findViewById(R.id.button);
        Intent intent = this.getIntent();
        models = (Model) intent.getParcelableExtra("note");
        if (models == null) {
            mode = MODE_CREATE;
        } else {
            mode = MODE_EDIT;
            editText.setText(String.valueOf(models.getA()));
            editText2.setText(String.valueOf(models.getB()));
            editText3.setText(String.valueOf(models.getC()));
        }

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                buttonSaveClicked();
            }
        });
    }

    private void buttonSaveClicked() {
        MyDataBaseHelper db = new MyDataBaseHelper(this);
        Double a = Double.parseDouble(editText.getText().toString());
        Double b = Double.parseDouble(editText2.getText().toString());
        Double c = Double.parseDouble(editText3.getText().toString());
        double cv = a + b + c;
        Double p = cv / 2;
        double dt = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        if (a.equals(0.0) || b.equals(0.0) || c.equals(0.0)) {
            Toast.makeText(getApplicationContext(),
                    "Please enter a , b, c", Toast.LENGTH_LONG).show();
            return;
        }

        if (mode == MODE_CREATE) {
            models = new Model(a, b, c);
            db.addTest(models);
        } else {
            models.setA(a);
            models.setB(b);
            models.setC(c);
            models.setCv(cv);
            models.setDt(dt);
            db.updateModel(models);
        }

        needRefresh = true;
        onBackPressed();
    }

    @Override
    public void finish() {

        // Create Intent
        Intent data = new Intent();

        // Request MainActivity refresh its ListView (or not).
        data.putExtra("needRefresh", needRefresh);

        // Set Result
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
}
