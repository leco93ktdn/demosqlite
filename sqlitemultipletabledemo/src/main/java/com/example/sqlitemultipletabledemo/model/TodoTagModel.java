package com.example.sqlitemultipletabledemo.model;

public class TodoTagModel {
    private int id;
    private long todoId;
    private long tagId;

    public TodoTagModel() {
    }

    public TodoTagModel(int id, long todoId, long tagId) {
        this.id = id;
        this.todoId = todoId;
        this.tagId = tagId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTodoId() {
        return todoId;
    }

    public void setTodoId(long todoId) {
        this.todoId = todoId;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }
}
