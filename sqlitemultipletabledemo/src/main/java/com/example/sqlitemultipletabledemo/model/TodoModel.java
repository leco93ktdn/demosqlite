package com.example.sqlitemultipletabledemo.model;

import androidx.annotation.NonNull;

public class TodoModel {
    int id;
    String note;
    int status;
    String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public TodoModel(int id, String note, int status, String created_at) {
        this.id = id;
        this.note = note;
        this.status = status;
        this.created_at = created_at;
    }

    public TodoModel(String note, int status) {
        this.note = note;
        this.status = status;
    }

    public TodoModel() {
    }

    @NonNull
    @Override
    public String toString() {
        return "ID:" + id + " - " + note;
    }
}
