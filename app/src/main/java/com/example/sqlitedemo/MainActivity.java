package com.example.sqlitedemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.sqlitedemo.model.Model;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton2;
    private static final int MENU_ITEM_VIEW = 111;
    private static final int MENU_ITEM_EDIT = 222;
    private static final int MENU_ITEM_CREATE = 333;
    private static final int MENU_ITEM_DELETE = 444;

    private static final int MY_REQUEST_CODE = 1000;
    private List<Model> modelList = new ArrayList<>();
    private SqlAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.listView);
        floatingActionButton2 = findViewById(R.id.floatingActionButton2);

        MyDataBaseHelper db = new MyDataBaseHelper(this);
        db.createDefaultTestsIfNeed();

        modelList = db.getAllModels();

        registerForContextMenu(recyclerView);

        LinearLayoutManager linearLayoutRecycleView = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutRecycleView);
        adapter = new SqlAdapter(this, modelList);
        recyclerView.setAdapter(adapter);
        registerForContextMenu(recyclerView);
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createModel();
            }
        });
    }

    private void createModel() {
        Intent intent = new Intent(this, AddEditModelActivity.class);

        // Start AddEditNoteActivity, (with feedback).
        this.startActivityForResult(intent, MY_REQUEST_CODE);
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//
//        menu.setHeaderTitle("Select The Action");
//
//        // groupId, itemId, order, title
//        menu.add(0, MENU_ITEM_VIEW, 0, "View Note");
//        menu.add(0, MENU_ITEM_CREATE, 1, "Create Note");
//        menu.add(0, MENU_ITEM_EDIT, 2, "Edit Note");
//        menu.add(0, MENU_ITEM_DELETE, 4, "Delete Note");
//    }

//    @Override
//    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        return super.onContextItemSelected(item);
//        int positon = item.getOrder();
//        Model selectedModel = recyclerView.get
//        if (item.getItemId() == MENU_ITEM_VIEW) {
//            Toast.makeText(getApplicationContext(), selectedModel.getId() + "cv = " + selectedModel.getCv() + "dt = " + selectedModel.getDt() + "", Toast.LENGTH_LONG).show();
//        } else if (item.getItemId() == MENU_ITEM_CREATE) {
//            Intent intent = new Intent(this, AddEditModelActivity.class);
//
//            // Start AddEditNoteActivity, (with feedback).
//            this.startActivityForResult(intent, MY_REQUEST_CODE);
//        } else if (item.getItemId() == MENU_ITEM_EDIT) {
//            Intent intent = new Intent(this, AddEditModelActivity.class);
//            intent.putExtra("note", selectedModel);
//
//            // Start AddEditNoteActivity, (with feedback).
//            this.startActivityForResult(intent, MY_REQUEST_CODE);
//        } else if (item.getItemId() == MENU_ITEM_DELETE) {
//            // Ask before deleting.
//            new AlertDialog.Builder(this)
//                    .setMessage(selectedModel.getId() + ". Are you sure you want to delete?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            deleteModel(selectedModel);
//                        }
//                    })
//                    .setNegativeButton("No", null)
//                    .show();
//        } else {
//            return false;
//        }
//        return super.onContextItemSelected(item);
//    }

    public void deleteModel(Model model) {
        MyDataBaseHelper db = new MyDataBaseHelper(this);
        db.deleteModel(model);
        modelList.remove(model);
        // Refresh ListView.
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == MY_REQUEST_CODE) {
            boolean needRefresh = data.getBooleanExtra("needRefresh", true);
            // Refresh ListView
            if (needRefresh) {
                this.modelList.clear();
                MyDataBaseHelper db = new MyDataBaseHelper(this);
                List<Model> list = db.getAllModels();
                this.modelList.addAll(list);


                // Notify the data change (To refresh the ListView).
                this.adapter.notifyDataSetChanged();
            }
        }
    }
}
