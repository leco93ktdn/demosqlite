package com.example.sqlitemultipletabledemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sqlitemultipletabledemo.adapter.TodoTagAdapter;
import com.example.sqlitemultipletabledemo.helper.DatabaseHelper;
import com.example.sqlitemultipletabledemo.model.TagModel;
import com.example.sqlitemultipletabledemo.model.TodoModel;
import com.example.sqlitemultipletabledemo.model.TodoTagModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvTodoTag;
    private Button btnAddTodo, btnAddTag;
    private DatabaseHelper db;
    private List<TodoTagModel> todoTagList;
    private Spinner spinner, spinner2;
    private ImageButton btnInsertTodoTag;
    private TextView tvSpinner, tvSpinner2;
    private TodoModel todoChoose;
    private TagModel tagChoose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DatabaseHelper(getApplicationContext());
        todoTagList = db.getAllTodoTags();

        initView();
        showTodoTagList(todoTagList);
        initEvent();
    }

    private void showTodoTagList(List<TodoTagModel> todoTagList) {
        Log.e("TODOTAGLIST", todoTagList.size() + "");
        if (todoTagList.size() > 0) {
            TodoTagAdapter adapter = new TodoTagAdapter(this, todoTagList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvTodoTag.setLayoutManager(linearLayoutManager);
//        adapter.setClickListener();
            rvTodoTag.setAdapter(adapter);
        }
    }

    private void initEvent() {
        btnAddTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TagActivity.class);
                startActivity(intent);
            }
        });

        btnAddTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ToDoActivity.class);
                startActivity(intent);
            }
        });

        setSpinner(spinner, 1);
        setSpinner(spinner2, 2);

        btnInsertTodoTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvSpinner.getText().toString().equals("") || tvSpinner2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please choose a todo and tag", Toast.LENGTH_LONG).show();
                } else {
                    db.createToDoTag(todoChoose.getId(), tagChoose.getId());
                    todoTagList = db.getAllTodoTags();
                    showTodoTagList(todoTagList);
                    tvSpinner.setText("");
                    tvSpinner2.setText("");
                }
            }
        });
    }

    private void setSpinner(final Spinner v, final int i) {
        final List<TodoModel> todoList = db.getAllToDos();
        List<TagModel> tagList = db.getAllTag();
        final ArrayAdapter adapter;
        final TextView viewText;
        if (i == 1) {
            adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, todoList);
            viewText = tvSpinner;
        } else {
            adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tagList);
            viewText = tvSpinner2;
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        v.setAdapter(adapter);
        v.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (i == 1) {
//                    todoChoose = (TodoModel) adapter.getItem(position);
                    todoChoose = (TodoModel) v.getSelectedItem();
                    viewText.setText(todoChoose.getNote());
                } else {
                    tagChoose = (TagModel) adapter.getItem(position);
                    viewText.setText(tagChoose.getTag_name());
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initView() {
        tvSpinner = findViewById(R.id.tvSpinner);
        tvSpinner2 = findViewById(R.id.tvSpinner2);
        rvTodoTag = findViewById(R.id.rvTodoTag);
        btnAddTodo = findViewById(R.id.btnAddTodo);
        btnAddTag = findViewById(R.id.btnAddTag);
        btnInsertTodoTag = findViewById(R.id.btnInsertTodoTag);
        spinner = findViewById(R.id.spinner);
        spinner2 = findViewById(R.id.spinner2);
    }
}

//
//        /*Create todos*/
//        TodoModel toDo1 = new TodoModel("Tạo CSDL", 0);
//        TodoModel toDo2 = new TodoModel("Design giao diện", 0);
//        TodoModel toDo3 = new TodoModel("Vẽ giao diện app Android", 0);
//        TodoModel toDo4 = new TodoModel("Code chức năng 1, 2, 3 app Android", 0);
//        TodoModel toDo5 = new TodoModel("Code chức năng 4, 5 app Android", 0);
//        TodoModel toDo6 = new TodoModel("Vẽ giao diện app iOS", 0);
//        TodoModel toDo7 = new TodoModel("Code chức năng 1, 2, 3 app iOS", 0);
//        TodoModel toDo8 = new TodoModel("Code chức năng 4, 5 app iOS", 0);
//        TodoModel toDo9 = new TodoModel("Vẽ giao diện web", 0);
//        TodoModel toDo10 = new TodoModel("Code chức năng 1, 2, 3 web", 0);
//        TodoModel toDo11 = new TodoModel("Code chức năng 4, 5 web", 0);
//
//        /*Create tags*/
//        TagModel tag1 = new TagModel("NV1");
//        TagModel tag2 = new TagModel("NV2");
//        TagModel tag3 = new TagModel("NV3");
//        TagModel tag4 = new TagModel("NV4");
//        TagModel tag5 = new TagModel("NV5");
//        TagModel tag6 = new TagModel("NV6");
//        TagModel tag7 = new TagModel("NV7");
//
//        /*insert tags in database*/
//        long tag1_id = db.createTag(tag1);
//        long tag2_id = db.createTag(tag2);
//        long tag3_id = db.createTag(tag3);
//        long tag4_id = db.createTag(tag4);
//        long tag5_id = db.createTag(tag5);
//        long tag6_id = db.createTag(tag6);
//        long tag7_id = db.createTag(tag7);
//
//        /*insert todos in database*/
//        long todo1_id = db.createNewToDo(toDo1, new Long[] { tag2_id });
//        long todo2_id = db.createNewToDo(toDo2, new Long[] { null });
//        long todo3_id = db.createNewToDo(toDo3, new Long[] { null });
//        long todo4_id = db.createNewToDo(toDo4, new Long[] { null });
//        long todo5_id = db.createNewToDo(toDo5, new Long[] { tag3_id });
//        long todo6_id = db.createNewToDo(toDo6, new Long[] { null });
//        long todo7_id = db.createNewToDo(toDo7, new Long[] { null });
//        long todo8_id = db.createNewToDo(toDo8, new Long[] { null });
//        long todo9_id = db.createNewToDo(toDo9, new Long[] { null });
//
//        Log.e("Todo Count", "Todo count: " + db.getToDoCount());
//
//        // "Post new Article" - assigning this under "Important" Tag
//        // Now this will have - "Androidhive" and "Important" Tags
//        db.createToDoTag(todo9_id, tag4_id);
//
//        // Getting all tag names
//        Log.d("Get Tags", "Getting All Tags");
//
//        List<TagModel> allTags = db.getAllTag();
//        for (TagModel tag : allTags) {
//            Log.d("Tag Name", tag.getTag_name());
//        }
//
//        // Getting all Todos
//        Log.d("Get Todos", "Getting All ToDos");
//
//        List<TodoModel> allToDos = db.getAllToDos();
//        for (TodoModel todo : allToDos) {
//            Log.d("ToDo", todo.getNote());
//        }
//
//        // Getting todos under "Watchlist" tag name
//        Log.d("ToDo", "Get todos under single Tag name");
//
//        List<TodoModel> tagsWatchList = db.getAllToDosByTag(tag3.getTag_name());
//        for (TodoModel todo : tagsWatchList) {
//            Log.d("ToDo Watchlist", todo.getNote());
//        }
//
//        // Deleting a To_Do
//        Log.d("Delete ToDo", "Deleting a Todo");
//        Log.d("Tag Count", "Tag Count Before Deleting: " + db.getToDoCount());
//
//        db.deleteToDo(todo8_id);
//
//        Log.d("Tag Count", "Tag Count After Deleting: " + db.getToDoCount());
//
//        // Deleting all Todos under "Shopping" tag
//        Log.d("Tag Count",
//                "Tag Count Before Deleting 'Shopping' Todos: "
//                        + db.getToDoCount());
//
//        db.deleteTag(tag1, true);
//
//        Log.d("Tag Count",
//                "Tag Count After Deleting 'Shopping' Todos: "
//                        + db.getToDoCount());
//
//        // Updating tag name
//        tag3.setTag_name("Movies to watch");
//        db.updateTag(tag3);
//
//        // Don't forget to close database connection
//        db.closeDB();
//