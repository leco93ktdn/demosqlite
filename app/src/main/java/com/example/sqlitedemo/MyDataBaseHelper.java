package com.example.sqlitedemo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;
import com.example.sqlitedemo.model.Model;

import java.util.ArrayList;
import java.util.List;

public class MyDataBaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "SQLite";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Test_Manage";
    private static final String TABLE_TEST = "Table_test";
    private static final String COLUMN_TEST_ID = "Test_Id";
    private static final String COLUMN_TEST_A = "Test_a";
    private static final String COLUMN_TEST_B = "Test_b";
    private static final String COLUMN_TEST_C = "Test_c";
    private static final String COLUMN_TEST_CV = "Test_CV";
    private static final String COLUMN_TEST_DT = "Test_DT";

    MyDataBaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "MyDatabaseHelper.onCreate ... ");
        String script = "CREATE TABLE " + TABLE_TEST + "("
                + COLUMN_TEST_ID + " Integer PRIMARY KEY," + COLUMN_TEST_A + " Double,"
                + COLUMN_TEST_B + " Double," + COLUMN_TEST_C + " Double,"
                + COLUMN_TEST_CV + " Double," + COLUMN_TEST_DT + " Double" + ")";
        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "MyDatabaseHelper.onUpgrade ... ");
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEST);

        // Create tables again
        onCreate(db);
    }

    private int getTestCount() {

        Log.i(TAG, "MyDatabaseHelper.getTestsCount ... ");
        String countQuery = "SELECT  * FROM " + TABLE_TEST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    void addTest(Model model) {
        Log.i(TAG, "MyDatabaseHelper.addTest ... " + "id = " + model.getId() + "; a = " + model.getA() + "; b = " + model.getB());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TEST_A, model.getA());
        values.put(COLUMN_TEST_B, model.getB());
        values.put(COLUMN_TEST_C, model.getC());
        values.put(COLUMN_TEST_CV, model.getCv());
        values.put(COLUMN_TEST_DT, model.getDt());

        db.insert(TABLE_TEST, null, values);
        db.close();
    }

    void createDefaultTestsIfNeed() {
        int count = this.getTestCount();
        if (count == 0) {
            Model model1 = new Model(3.0, 3.0, 3.0);
            addTest(model1);
        }
    }

    public Model getModel(int id) {

        Log.i(TAG, "MyDatabaseHelper.getTest ... " + id);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(
                TABLE_TEST,
                new String[]{COLUMN_TEST_ID, COLUMN_TEST_A, COLUMN_TEST_B, COLUMN_TEST_C, COLUMN_TEST_CV, COLUMN_TEST_DT},
                COLUMN_TEST_ID + "=?",
                new String[]{String.valueOf(id)},
                null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        Model model = new Model(
                Double.parseDouble(cursor.getString(1)),
                Double.parseDouble(cursor.getString(2)),
                Double.parseDouble(cursor.getString(3)));
        return model;
    }

    List<Model> getAllModels() {
        Log.i(TAG, "MyDatabaseHelper.getAllModels ... ");
        List<Model> modelList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TEST;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Model model = new Model();
                model.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_TEST_ID)));
                model.setA(cursor.getDouble(cursor.getColumnIndex(COLUMN_TEST_A)));
                model.setB(cursor.getDouble(cursor.getColumnIndex(COLUMN_TEST_B)));
                model.setC(cursor.getDouble(cursor.getColumnIndex(COLUMN_TEST_C)));
                model.setCv(cursor.getDouble(cursor.getColumnIndex(COLUMN_TEST_CV)));
                model.setDt(cursor.getDouble(cursor.getColumnIndex(COLUMN_TEST_DT)));
                modelList.add(model);
            } while (cursor.moveToNext());
        }
        return modelList;
    }

    public int getModelsCount() {
        Log.i(TAG, "MyDatabaseHelper.getTestCount ... ");
        String countQuery = "SELECT * FROM " + TABLE_TEST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    int updateModel(Model model) {
        Log.i(TAG, "MyDatabaseHelper.updateNote ... " + model.getId());
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TEST_A, model.getA());
        values.put(COLUMN_TEST_B, model.getB());
        values.put(COLUMN_TEST_C, model.getC());
        values.put(COLUMN_TEST_CV, model.getCv());
        values.put(COLUMN_TEST_DT, model.getDt());

        return db.update(TABLE_TEST, values, COLUMN_TEST_ID + " = ?",
                new String[]{String.valueOf(model.getId())});
    }

    void deleteModel(Model model) {
        Log.i(TAG, "MyDatabaseHelper.deleteModel ... " + model.getId());
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TEST, COLUMN_TEST_ID + " = ?", new String[]{String.valueOf(model.getId())});
        db.close();
    }
}
