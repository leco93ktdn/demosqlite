package com.example.sqlitemultipletabledemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitemultipletabledemo.R;
import com.example.sqlitemultipletabledemo.model.TagModel;

import java.util.ArrayList;
import java.util.List;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.MyViewHolder>{

    private List<TagModel> data;
    private Context mContext;
    private TagAdapter.ItemClickListener mClickListener;

    public TagAdapter(Context context, List<TagModel> data) {
        this.mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewStatus = LayoutInflater.from(mContext).inflate(R.layout.item_tag, parent, false);
        return new MyViewHolder(viewStatus);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TagModel model = data.get(position);
        holder.tvId.setText(String.valueOf(model.getId()));
        holder.tvBody.setText(String.valueOf(model.getTag_name()));
        holder.tvDate.setText(String.valueOf(model.getCreated_at()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvDate;
        private TextView tvBody;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvId);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvBody = itemView.findViewById(R.id.tvBody);
        }
    }


    void addData(TagModel item) {
        data.add(item);
    }

    // allows clicks events to be caught
    public void setClickListener(TagAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    //parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemAdapterClick(View view, int position);
    }
}
