package com.example.sqlitemultipletabledemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitemultipletabledemo.R;
import com.example.sqlitemultipletabledemo.model.TodoTagModel;

import java.util.ArrayList;
import java.util.List;

public class TodoTagAdapter extends RecyclerView.Adapter<TodoTagAdapter.MyViewHolder>{

    private List<TodoTagModel> data;
    private Context mContext;
    private TodoTagAdapter.ItemClickListener mClickListener;

    public TodoTagAdapter(Context context, List<TodoTagModel> data) {
        this.mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewStatus = LayoutInflater.from(mContext).inflate(R.layout.item_todo_tag, parent, false);
        return new MyViewHolder(viewStatus);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TodoTagModel model = data.get(position);
        holder.tvId.setText(String.valueOf(model.getId()));
        holder.tvTodoId.setText(String.valueOf(model.getTodoId()));
        holder.tvTagId.setText(String.valueOf(model.getTagId()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId;
        private TextView tvTodoId;
        private TextView tvTagId;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tvId);
            tvTodoId = itemView.findViewById(R.id.tvTodoId);
            tvTagId = itemView.findViewById(R.id.tvTagId);
        }
    }


    void addData(TodoTagModel item) {
        data.add(item);
    }

    // allows clicks events to be caught
    public void setClickListener(TodoTagAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    //parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemAdapterClick(View view, int position);
    }
}
