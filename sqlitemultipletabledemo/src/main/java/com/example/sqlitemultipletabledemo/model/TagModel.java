package com.example.sqlitemultipletabledemo.model;

public class TagModel {
    private int id;
    private String tag_name;
    private String created_at;

    public TagModel(int id, String tag_name, String created_at) {
        this.id = id;
        this.tag_name = tag_name;
        this.created_at = created_at;
    }

    public TagModel(int id, String tag_name) {
        this.id = id;
        this.tag_name = tag_name;
    }

    public TagModel(String tag_name) {
        this.tag_name = tag_name;
    }

    public TagModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag_name() {
        return tag_name;
    }

    public void setTag_name(String tag_name) {
        this.tag_name = tag_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "ID: " + id + " - " + tag_name;
    }
}
