package com.example.sqlitedemo.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Model implements Parcelable {
    private Integer id = 0;
    private Double a = 0.0;
    private Double b = 0.0;
    private Double c = 0.0;
    private Double cv = 0.0;
    private Double dt = 0.0;

    public Model(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Model(int id, double a, double b, double c) {
        this.id = id;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Model(int id, double a, double b, double c, double cv, double dt) {
        this.id = id;
        this.a = a;
        this.b = b;
        this.c = c;
        this.cv = cv;
        this.dt = dt;
    }


    public Model() {
    }

    private Model(Parcel in) {
        id = in.readInt();
        a = in.readDouble();
        b = in.readDouble();
        c = in.readDouble();
        cv = in.readDouble();
        dt = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeDouble(a);
        dest.writeDouble(b);
        dest.writeDouble(c);
        dest.writeDouble(cv);
        dest.writeDouble(dt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Model> CREATOR = new Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }

    public double getCv() {
        return cv;
    }

    public void setCv(double cv) {
        this.cv = a + b + c;
    }

    public double getDt() {
        return dt;
    }

    public void setDt(double dt) {
        double p = cv / 2;
        this.dt = Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

//    public Double cv() {
//        return a + b + c;
//    }

//    public Double dt() {
//        double p = cv / 2;
//        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
//    }
}
