package com.example.sqlitemultipletabledemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.sqlitemultipletabledemo.R;
import com.example.sqlitemultipletabledemo.adapter.TodoAdapter;
import com.example.sqlitemultipletabledemo.adapter.TodoTagAdapter;
import com.example.sqlitemultipletabledemo.helper.DatabaseHelper;
import com.example.sqlitemultipletabledemo.model.TodoModel;
import com.example.sqlitemultipletabledemo.model.TodoTagModel;

import java.util.ArrayList;
import java.util.List;

public class ToDoActivity extends AppCompatActivity {
    private ImageButton btnBack;
    private EditText editText;
    private Button insert, update, delete;
    private RecyclerView rvTodos;
    private List<TodoModel> todoList = new ArrayList<>();
    private DatabaseHelper db;
    TodoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);

        db = new DatabaseHelper(getApplicationContext());

        initView();
        initData();
        initEvent();
    }

    private void initData() {
        todoList = db.getAllToDos();
        Log.e("TODOLIST", todoList.size() + "");
        addData(todoList);
    }

    private void addData(List<TodoModel> todoList) {
        if (todoList.size() > 0) {
            adapter = new TodoAdapter(this, todoList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            rvTodos.setLayoutManager(linearLayoutManager);
//        adapter.setClickListener();
            rvTodos.setAdapter(adapter);
        }
    }

    private void initEvent() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Enter your todo", Toast.LENGTH_LONG).show();
                }
                else {
                    String note = editText.getText().toString();
                    TodoModel todoItem = new TodoModel(note, 0);
                    db.createNewToDo(todoItem, null);
                    todoList = db.getAllToDos();
                    addData(todoList);
                    editText.setText("");
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (TodoModel model : todoList) {
                    db.deleteToDo(model.getId());
                }
                todoList.clear();
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void initView() {
        btnBack = findViewById(R.id.btnBack);
        editText = findViewById(R.id.editText);
        insert = findViewById(R.id.button);
        update = findViewById(R.id.button2);
        delete = findViewById(R.id.button3);
        rvTodos = findViewById(R.id.rvTodos);
    }
}
