package com.example.sqlitemultipletabledemo.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.sqlitemultipletabledemo.model.TagModel;
import com.example.sqlitemultipletabledemo.model.TodoModel;
import com.example.sqlitemultipletabledemo.model.TodoTagModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "LE CO";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Table Names
    private static final String TABLE_TODO = "todos";
    private static final String TABLE_TAG = "tags";
    private static final String TABLE_TODO_TAG = "todo_tags";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_at";

    // TO_DO Table - column names
    private static final String KEY_TODO = "todo";
    private static final String KEY_STATUS = "status";

    // TAGS Table - column names
    private static final String KEY_TAG_NAME = "tag_name";

    // TO_DO_TAGS Table - column names
    private static final String KEY_TODO_ID = "todo_id";
    private static final String KEY_TAG_ID = "tag_id";

    /*To_do table create statement*/
    private static final String CREATE_TABLE_TODO = "CREATE TABLE " + TABLE_TODO + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," + KEY_TODO + " TEXT," + KEY_STATUS + " INTEGER," +
            KEY_CREATED_AT + " DATETIME" + ")";

    // Tag table create statement
    private static final String CREATE_TABLE_TAG = "CREATE TABLE " + TABLE_TAG + "(" +
            KEY_ID + " INTEGER PRIMARY KEY," + KEY_TAG_NAME + " TEXT," + KEY_CREATED_AT +
            " DATETIME)";

    // todo_tag table create statement
    private static final String CREATE_TABLE_TODO_TAG = "CREATE TABLE "
            + TABLE_TODO_TAG + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_TODO_ID + " INTEGER," + KEY_TAG_ID + " INTEGER,"
            + KEY_CREATED_AT + " DATETIME" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TODO);
        db.execSQL(CREATE_TABLE_TAG);
        db.execSQL(CREATE_TABLE_TODO_TAG);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String DROP = "DROP TABLE IF EXISTS ";
        db.execSQL(DROP + TABLE_TODO);
        db.execSQL(DROP + TABLE_TAG);
        db.execSQL(DROP + TABLE_TODO_TAG);
        // create new tables
        onCreate(db);
    }

    public long createNewToDo(TodoModel todoModel, Long[] tag_ids) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TODO, todoModel.getNote());
        values.put(KEY_STATUS, todoModel.getStatus());
        values.put(KEY_CREATED_AT, getDateTime());

        long todo_id = db.insert(TABLE_TODO, null, values);
        if (tag_ids != null) {
//            for (long tag_id : tag_ids) {
//                createToDoTag(todo_id, tag_id);
//            }
        }
        return todo_id;
    }

    public int getToDoCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TODO;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;
    }

    public TodoModel getTodo(long todo_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_TODO + " WHERE "
                + KEY_ID + " = " + todo_id;
        Log.e(LOG, selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        TodoModel todoModel = new TodoModel();
        todoModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
        todoModel.setNote(cursor.getString(cursor.getColumnIndex(KEY_TODO)));
        todoModel.setStatus(cursor.getInt(cursor.getColumnIndex(KEY_STATUS)));
        todoModel.setCreated_at(cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT)));
        return todoModel;
    }

    public List<TodoModel> getAllToDos() {
        List<TodoModel> listTodo = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TODO;
        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TodoModel todoItem = new TodoModel();
                todoItem.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                todoItem.setNote(cursor.getString(cursor.getColumnIndex(KEY_TODO)));
                todoItem.setCreated_at(cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT)));

                listTodo.add(todoItem);
            } while (cursor.moveToNext());
        }
        return listTodo;
    }

    public List<TodoModel> getAllToDosByTag(String tag_name) {
        List<TodoModel> todoList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TODO + " td, " + TABLE_TAG + " tg, " +
                TABLE_TODO_TAG + " tt WHERE tg." + KEY_TAG_NAME + " = '" + tag_name + "'" + " AND tg." + KEY_ID
                + " = " + "tt." + KEY_TAG_ID + " AND td." + KEY_ID + " = "
                + "tt." + KEY_TODO_ID;
        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TodoModel todoItem = new TodoModel();
                todoItem.setId(cursor.getInt((cursor.getColumnIndex(KEY_ID))));
                todoItem.setNote(cursor.getString((cursor.getColumnIndex(KEY_TODO))));
                todoItem.setCreated_at(cursor.getString((cursor.getColumnIndex(KEY_CREATED_AT))));
                todoList.add(todoItem);
            }while (cursor.moveToNext());
        }
        return todoList;
    }

    public int updateToDo(TodoModel todoModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TODO, todoModel.getNote());
        values.put(KEY_STATUS, todoModel.getStatus());
        return db.update(TABLE_TODO, values, KEY_ID + " = ?",
                new String[]{String.valueOf(todoModel.getId())});
    }

    public void deleteToDo(long todo_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TODO, KEY_ID + " = ? ",
                new String[]{String.valueOf(todo_id)});
    }

    public long createTag(TagModel tagModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TAG_NAME, tagModel.getTag_name());
        values.put(KEY_CREATED_AT, getDateTime());
        return db.insert(TABLE_TAG, null, values);
    }

    public List<TagModel> getAllTag() {
        List<TagModel> tagList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TAG;
        Log.e(LOG, selectQuery);

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                TagModel tagItem = new TagModel();
                tagItem.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                tagItem.setTag_name(cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME)));
                tagItem.setCreated_at(cursor.getString(cursor.getColumnIndex(KEY_CREATED_AT)));
                tagList.add(tagItem);
            } while (cursor.moveToNext());
        }
        return tagList;
    }

    public int updateTag(TagModel tagModel) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TAG_NAME, tagModel.getTag_name());
        int query = db.update(TABLE_TAG, values, KEY_ID + " = ? ",
                new String[]{String.valueOf(tagModel.getId())});
        Log.e(LOG, String.valueOf(query));
        return query;

    }

    public void deleteTag(TagModel tagModel, boolean shoud_delete_all_tag_todos) {
        SQLiteDatabase db = getWritableDatabase();

        if (shoud_delete_all_tag_todos) {
            List<TodoModel> allTagToDos = getAllToDosByTag(tagModel.getTag_name());
            for (TodoModel todoModel : allTagToDos) {
                deleteToDo(todoModel.getId());
            }
        }

        db.delete(TABLE_TAG, KEY_ID + " = ? ",
                new String[]{String.valueOf(tagModel.getId())});
    }

    public long createToDoTag(long todoId, long tagId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TODO_ID, todoId);
        values.put(KEY_TAG_ID, tagId);
        values.put(KEY_CREATED_AT, getDateTime());
        long id = db.insert(TABLE_TODO_TAG, null, values);
        Log.e(LOG, String.valueOf(id));
        return id;
    }

    /*Removing Tag of To_do*/
    public int deleteToDoTag(long id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_TODO_TAG,KEY_ID + " = ? ",
                new String[]{String.valueOf(id)});
    }

    /*Changing the tag of to_do*/
    public int updateToDoTag(long id, long tag_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TAG_ID, tag_id);

        return db.update(TABLE_TODO, values, KEY_ID + " = ?",
                new String[]{String.valueOf(id)});
    }

    public List<TodoTagModel> getAllTodoTags() {
        List<TodoTagModel> listTodoTag = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_TODO_TAG;
        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                TodoTagModel todotagItem = new TodoTagModel();
                todotagItem.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                todotagItem.setTagId(cursor.getLong(cursor.getColumnIndex(KEY_TAG_ID)));
                todotagItem.setTodoId(cursor.getLong(cursor.getColumnIndex(KEY_TODO_ID)));

                listTodoTag.add(todotagItem);
            } while (cursor.moveToNext());
        }
        return listTodoTag;
    }

    public void closeDB() {
        SQLiteDatabase db = getReadableDatabase();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
